<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
require_once '/home/eliecer/Documentos/la-bah-a-del-pirata/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPConnection;
use App\Movie;
use DB;

class Receiver extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'receiver';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'isplay an inspiring quote';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

      $connection = new AMQPConnection('localhost', 5672, 'guest', 'guest');
      $channel = $connection->channel();

      $channel->queue_declare('videos_queue', false, false, false, false);

      echo ' * Waiting for messages. To exit press CTRL+C', "\n";

      $callback = function($msg){

          $data = json_decode($msg->body, true);
          $link=$data['link'];
        //  $f ='youtube-dl -o "/home/eliecer/Documentos/LaBahiadelPirata/public/videos/%(title)s.%(ext)s" '.$link;
         $f ='youtube-dl -o "/home/eliecer/Documentos/la-bah-a-del-pirata/public/videos/ '.$data['id'].".mp4".'" '.$link;
        //  $f ='youtube-dl -o "/home/eliecer/Documentos/LaBahiadelPirata/public/videos/ funny_video.mp4"'.$link;
         $name ='youtube-dl --get-filename -o "%(title)s.%(ext)s" '.$link;
         $title= shell_exec($name);
            shell_exec($f);
          DB::table('movies')
            ->where('id', $data['id'])
            ->update(['status' => 1,'name'=>$title]);

          $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

      };

      $channel->basic_qos(null, 1, null);
      $channel->basic_consume('videos_queue', '', false, false, false, false, $callback);

      while(count($channel->callbacks)) {
          $channel->wait();
      }
      $channel->close();
      $connection->close();

    }
}
