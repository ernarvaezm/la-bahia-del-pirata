<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Jobs\addMessageQueue;
use Auth;
use App\Movie;
use App\Thread;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;
use App\Repositories\DropboxStorageRepository;
use Storage;
class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {   $movies =  DB::table('movies')->where('user_id',Auth::user()->id)->get();
    return view('home',compact('movies'));
  }
  /**
  * add one message to the queue
  */
  public function add(Request $request)
  {
    $thread=Thread::findOrFail(1);
    if ($thread->threads>=5) {
      return redirect('home')->with('status', ' you have 5 downloads  running ,Please wait while one of your download finishes!');

    }else {
      $thread->threads=$thread->threads+1;
      $thread->save();
      $movie = new Movie;
      $movie->user_id=Auth::user()->id;
      $movie->link=$request->link;
      $movie->save();
      $this->dispatch(new addMessageQueue($movie->link ,$movie->id,Auth::user()->id));
      return redirect('/home');
    }

  }
  /**
  * Descarga el video seleccionado
  */
  public function download($id)
  {

    $movie=Movie::findOrFail($id);
    if ($movie->user_id==Auth::user()->id) {
      $file= '/home/eliecer/Documentos/la-bah-a-del-pirata/public/videos/'.$movie->id.'.mp4';
      return response()->download($file,$movie->name,['Content-Type','application/pdf']);
    }else {
      return redirect('home')->with('status', 'This video has another owner');
    }

  }
  /**
  * obtiene los videos y los retorna en formato json
  */
  public function videos()
  {   $movies = DB::table('movies')->where('user_id',Auth::user()->id)->get();
    return response()->json(['movies' => $movies]);

  }
  /**
   * show view to upload the file
   */
  public function file()
  {
    return view('dropbox.fileUpload');
  }
  /**
   * upload the video to dropbox
   *
   */
  public function upload_to_dropbox(DropboxStorageRepository $connection)
  {
    $filesystem = $connection->getConnection();
    $file = Input::file('image');
    $filesystem->put($file->getClientOriginalName(), File::get($file));


    // Calling to controller file method and redirect to form view page
    // After redirection user getting acknowledgment of success message
    return Redirect::to('home')->with('status', 'Upload successfully');
  }
}
