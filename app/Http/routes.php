<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('/getMovies', 'HomeController@videos');
Route::post('send', 'HomeController@add');
Route::get('/download/{id}','HomeController@download');
Route::post('upload', 'HomeController@upload_to_dropbox');
Route::get('upload', 'HomeController@file');
Route::resource('user','UserController');
Route::get('destroy', 'UserController@destroy');
