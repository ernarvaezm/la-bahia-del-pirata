<?php

namespace App\Jobs;
require_once '/home/eliecer/Documentos/la-bah-a-del-pirata/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class addMessageQueue extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $request;
    protected $id;
    protected $id_user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $request,$id ,$id_user)
    {
        $this->request=$request;
        $this->id_user=$id_user;
        $this->id=$id;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->release(10);
      $connection = new AMQPConnection('localhost', 5672, 'guest', '12345678');
      $channel = $connection->channel();
      $channel->queue_declare('videos_queue', false, false, false, false);
      $array = array(
          "id" => $this->id,
          "id_user"=> $this->id_user,
          "link" => $this->request,
      );
      $data = json_encode($array);

      $msg = new AMQPMessage($data, array('delivery_mode' => 2));
      $channel->basic_publish($msg, '', 'videos_queue');

      $channel->close();
      $connection->close();
    }
}
