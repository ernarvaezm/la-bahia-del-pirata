@extends('layouts.app')
@section('content')
<div class="row">
  <div class="col-md-4">
    @if (session('status'))
  <div class="alert alert-success">
      {{ session('status') }}
  </div>
@endif
  </div>
  <div class="col-md-4">
    <div class="panel panel-default">
  <div class="panel-heading">Upload to Dropbox</div>
  <div class="panel-body">
    {!! Form::open(array('url'=>'upload','method'=>'POST', 'files'=>true ,'class'=>'navbar-form navbar-left')) !!}
     <div class="form-group">
    {!! Form::file('image', array('multiple'=>false)) !!}
      </div>

    {!! Form::submit('Send', array('class'=>'btn btn-warning')) !!}
    {!! Form::close() !!}
  </div>
</div>






  </div>
  <div class="col-md-4">
  </div>
</div>
    @endsection
