@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>

            <div class="panel-body">
              <table id="example" class="table table-striped" cellspacing="0" width="50%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Link</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($movies as $mov)
                  <tr>
                    <td>{{$mov->id}}</td>
                    <td>{{$mov->link}}</td>

                  <?php $status=$mov->status?>

                      <td>
                        @if($status==4)
                        <img src="381.gif" class="img-rounded" alt="Cinque Terre" >
                        @endif
                        @if($status==3)
                        <img src="fail.png" class="img-rounded" alt="Cinque Terre" >
                        @endif

                        @if($status==0)
                        <a href='/download/{{$mov->id}}'>
                    <button type="button" class="btn btn-info">Downloaded</button>
                        </a>
                        <a href='/upload'>
                    <button type="button" class="btn btn-warning">Save dropbox</button>
                        </a>

                        @endif

                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <form  action="/send"class="form-inline" method="post" >
              {{ csrf_field() }}
        <div class="form-group">
          <div class="input-group">
            <div class="input-group-addon">Link</div>
            <input type="text" name="link" class="form-control" id="exampleInputAmount" >
          </div>
        </div>
        <button type="submit" class="btn btn-primary">Add to queue</button>
      </form>
      @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
    </div>

  </div>

</div>

@endsection
