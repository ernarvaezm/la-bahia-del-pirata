@extends('layouts.app')
@section('content')
<div class="row">
      <div class="col-md-6 col-md-offset-2">
<div class="panel panel-default">
      <div class="panel-heading"><h2>Perfil</h2></div>
      <div class="panel-body">
          <form action='/user/{{$user->id}}' method="POST">
              <input type="hidden" name="_method" value="PUT">
              <div class="col-xs-12 ">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <label for="name">Nombre:</label>
                  <input class="form-control" required="required" name="name" type="text" id="name" value="{{$user->name}}">
                  <label for="name">Usuario:</label>
                  <input class="form-control" required="required" name="email" type="text"  readonly="" id="email" value="{{$user->email}}">
                  <br>
                  <label for="password1">Nueva Contraseña:</label>
                  <input name="password" type="password"  id="password2" class="form-control"/>
                  <br>
                  {!! Form::submit('Guardar', ['class' => 'btn btn-primary form-control']) !!}
              </div>
          </form>
      </div>
          <a href="{{ url('destroy') }}"><i class="fa fa-btn glyphicon glyphicon-cog"></i>DELETE</a>
        </div>
          </div>
@endsection
